# Kyss

Kyss is a Python application builder that's help you to create and distribute apps based on Pywebview (https://pywebview.flowrl.com/)

Kyss are used in Ryztart, The Ryzom Launcher (https://gitlab.com/ryzom/ryztart)

## How to install with pip
```pip install  'git+https://gitlab.com/ulukyn/kyss.git#egg=kyss[DIST]'```
or in editable mode
```pip install -e .```
