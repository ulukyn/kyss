########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import time
import queue
import random
import appdirs
import pkgutil
import webview
import platform
import threading
import subprocess
import configparser
from kyss.module import KyssModule
from importlib import import_module
from pathlib import Path, PurePosixPath, PureWindowsPath


class KyssApp(KyssModule):
	dom_ready = False

	def toggleFullscreen(self, param):
		webview.windows[0].toggle_fullscreen()

	def stop(self):
		self.threadQueue.put(("STOP","","", False))
	
	def exit(self):
		self.stop()
		os._exit(1)

	def _init(self):
		self.threadQueue = queue.Queue()

	def initWebview(self, webview, window):
		self.links = {}
		KyssModule.webview = webview
		KyssModule.main_window = window

	def loadModules(self):
		self.modules_config = configparser.ConfigParser()
		self.modules_config.read(os.path.join("data", "modules.ini"))
		KyssModule.modules["app"] = self
		KyssModule.setApp(self, self.webview)
		self.getTranslations("app")

		for section in self.modules_config.sections():
			if section == "translations":
				for name in list(self.modules_config.items(section)):
					self.getTranslations(name[0], True)
			elif section != "menu":
				for name in list(self.modules_config.items(section)):
					name = name[0]
					if not name in KyssModule.modules:
						self.log("Loading module "+name)
						module = import_module(name)
						name = name.lower()
						if hasattr(module, "instance"):
							KyssModule.modules[name] = module.instance

	def stopModules(self):
		for name, module in KyssModule.modules.items():
			self.log("Stoping {}...".format(name))
			module.stop()

	def getModule(self, name):
		if name in KyssModule.modules:
			return KyssModule.modules[name]
		return None

	def run(self, module, method, *args):
		url = KyssModule.main_window.get_current_url()
		self.log("{}:{} {}".format(module, method, args))
		if module in KyssModule.modules:
			KyssModule.modules[module].call(method, args)
		else:
			self.error("No module found: {}".format(module))
		return False

	def getModuleCss(self, name):
		self.log("Setup CSS for %s" % name)
		if name in KyssModule.modules:
			css_module = KyssModule.modules[name].getCssFile()
			if css_module:
				self.log("Setup CSS for %s" % name)
				if KyssModule.base_url:
					return "<link href=\"{}{}\" rel=\"stylesheet\">\n".format(KyssModule.base_url, KyssModule.modules[name].getCssFile())
				else:
					return "<link href=\"file:{}\" rel=\"stylesheet\">\n".format(KyssModule.modules[name].getCssFile())
		return ""

	def getHtmlZonesAndCss(self):
		self.activeZoneModules = {"main": []}
		modules_zones = {}
		final_zones = {"header": {}, "main": {}, "popup": {}, "footer": {}}
		
		css = ""
		for name, module in KyssModule.modules.items():
			css += self.getModuleCss(name)
			zones = module.getUsedZones()
			for zone in final_zones:
				if zone in zones:
					zone_prio = zones[zone]
					if not zone_prio in final_zones[zone]:
						final_zones[zone][zone_prio] = []
						modules_zones[zone] = []
					self.log("Added zone {} for module {}".format(zone, name))
					final_zones[zone][zone_prio].append("<div class=\"zone\" data-display=\"block\" style=\"display: none\" id=\"{}-{}\"></div>".format(zone, name))
					modules_zones[zone].append(name)

		zones_html = {}
		for zone in final_zones:
			zones_html[zone] = ""
			sorted_zones = sorted(final_zones[zone])
			for s_zone in sorted_zones:
				htmls = final_zones[zone][s_zone]
				for html in htmls:
					zones_html[zone] += html

		self.selectedMenu = ""
		zones_html["menu"] = ""
		if "menu" in self.modules_config:
			for name, value in self.modules_config["menu"].items():
				svalue = value.split(" ")
				menu_config = {}
				menu_config[svalue[0]+"?"] = True # Enable the menu button type
				menu_config["name"] = name
				menu_config["tooltip"] = self._("ryzom_content_manager."+name)
				if len(svalue) > 1 :
					menu_config["action"] = svalue[1]
				if len(svalue) > 2 :
					menu_config["icon"] = svalue[2]
				zones_html["menu"] += self.getTemplateV2("/menu_button", menu_config)

		self.current_page = ""
		for section in self.modules_config.sections():
			if self.current_page == "" and section[:5] == "main:":
				self.current_page = section[5:]
		
		self.modules_zones = modules_zones
		self.zones = zones_html
		self.css = css

	def setupZone(self, zone, ignore_modules=[]):
		self.setE(zone, self.zones[zone])
		for module in self.modules_zones[zone]:
			if not module in ignore_modules and module in self.modules:
				self.modules[module].setup()
				self.showE(zone+"-"+module)
		
	def addModuleZone(self, zone, module, prepend=False):
		if prepend:
			self.prependToE(zone, "<div class=\"zone\" data-display=\"block\" style=\"display: none\" id=\"{}-{}\"></div>".format(zone, module))
		else:
			self.addToE(zone, "<div class=\"zone\" data-display=\"block\" style=\"display: none\" id=\"{}-{}\"></div>".format(zone, module))

	def showModulesInZone(self, zone, modules):
		for module in modules:
			self.log("Show {} {} ".format(zone, module))
			self.showE(zone+"-"+module)
			self.activeZoneModules[zone].append(module)

	def hideModulesInZone(self, zone):
		for module in self.activeZoneModules[zone]:
			self.hideE(zone+"-"+module)
		self.activeZoneModules[zone] = []

	def selectMenu(self, module):
		if self.selectedMenu:
			self.callE("menu-"+self.selectedMenu, "classList.remove(\"selected\")")
		self.selectedMenu = module
		self.callE("menu-"+self.selectedMenu, "classList.add(\"selected\")")

	def call_Refresh(self, width=None, height=None):
		if width and height:
			#Resize required to fix a bug with dpi
			self.window.resize(width, height)

		if KyssModule.main_window._is_http_server:
			KyssModule.base_url = webview.window.resolve_url("file://", should_serve=True)+"/"
		else:
			if platform.system() ==  "Darwin" and not os.path.isdir(".."+os.sep+".git"):
				KyssModule.base_url = "app/"
			else:
				KyssModule.base_url = ""
		
		self.getHtmlZonesAndCss()
		zones_html = self.zones

		langs = ""
		if hasattr(KyssModule, "lang") and  hasattr(KyssModule, "langs"):
			langs = "<div class=\"dropdown\"><button class=\"dropbtn\"><img src=\"{}data/icons/{}.svg\" width=\"35px\" height=\"19px\" /></button><div class=\"dropdown-content\">".format(KyssModule.base_url, KyssModule.lang)
			for lang in KyssModule.langs:
				if lang != "wk" and lang != "default" and lang != KyssModule.lang:
					langs += "<a href=\"#\" onclick=\"pywebview.api.run('app', 'SetLang', '{}');\"><img src=\"{}data/icons/{}.svg\" width=\"35px\" height=\"19px\" /></a>".format(lang, KyssModule.base_url, lang)
			langs += "</div></div>"

		# Fill Templates
		with open(os.path.join("data", "html", "header.html")) as f:
			header_html = f.read().replace("{{modules}}", zones_html["header"])

		with open(os.path.join("data", "html", "footer.html")) as f:
			footer_html = f.read().replace("{{modules}}", zones_html["footer"])

		with open(os.path.join("data", "html", "index.html")) as f:
			html = f.read()

		html = html.replace("{{header}}", header_html)
		html = html.replace("{{footer}}", footer_html)
		html = html.replace("{{main}}", zones_html["main"])
		html = html.replace("{{menu}}", zones_html["menu"])
		html = html.replace("{{popup}}", zones_html["popup"])
		html = html.replace("{{version}}", KyssModule.VERSION)
		html = html.replace("{{author}}", KyssModule.AUTHOR)
		html = html.replace("{{langs}}", langs)
		html = html.replace("{{css}}", self.css)
		
		if KyssModule.base_url:
			html = html.replace("file:", KyssModule.base_url)

		KyssModule.main_window.load_html(html)

	def call_OpenMenu(self):
		self.showE("menu")

	def call_SwitchMenu(self):
		self.switchE("menu")

	def call_ShowMainPage(self, page):
		if "main:"+page in self.modules_config:
			modules = self.modules_config["main:"+page].keys()
			self.hideModulesInZone("main")
			self.showModulesInZone("main", modules)
			
			for key in modules:
				self.log("setup {}".format(key))
				self.getModule(key).setup()
	
	def call_ShowUrlInNewWindow(self, url):
		# TODO : fix title
		KyssModule.webview.create_window("Ryzom Chat", url=url)

	def call_LoadModuleInNewWindow(self, module, title):
		css = self.getModuleCss(module)
		vals = {"title": title, css: css,
			"content": "<div class=\"zone\" data-display=\"none\" style=\"display: block\" id=\"{}-{}\"></div>".format("main", module),
			"popup": "<div class=\"zone\" data-display=\"block\" style=\"display: none\" id=\"{}-{}\"></div>".format("popup", module),
			} 
		html = self.getTemplateV2("/flat", vals)
		window = KyssModule.webview.create_window(title, js_api=self, html=html)
		KyssModule.addWindow(window)
		window.events.loaded += lambda: self.onLoadedModuleInNewWindow(module)
		window.events.closed += lambda: self.onClosedModuleInNewWindow(module)
		self.hideE("popup")

	def onLoadedModuleInNewWindow(self, module):
		self.getModule(module).setup()

	def onClosedModuleInNewWindow(self, module):
		self.getModule(module).setup()
		

	def call_OpenFolder(self, path):
		if platform.system() ==  "Darwin":
			subprocess.run(["open", "--", path])
		elif platform.system() == "Linux":
			subprocess.run(["xdg-open", path])
		else:
			subprocess.run(["explorer", str(Path(path))])
	
	def call_SetLang(self, lang):
		KyssModule.lang = lang
		self.reloadTranslations()
		self.stop()
		KyssModule.main_window.events.loaded += self.onLoaded
		self.call_Refresh()

	def onLoaded(self):
		self.dom_ready = True
		KyssModule.main_window.events.loaded -= self.onLoaded
		
		self.log("Dom ready !")
		# wait some time for slow computers
		time.sleep(0.5)
		self.log("Starting...")
		self._onDomReady()
		self.setupE("body", "style.backgroundImage", "\"url('{}/data/bg/{}.jpg')\"".format(KyssModule.base_url, random.choice([1, 2, 3, 4])))
		
		self.modules_threads = {}
		for module in KyssModule.modules:
			self.log("Starting module {}.setup()".format(module))
			#t = threading.Thread(target=KyssModule.modules[module].setup)
			#self.modules_threads[module] = t
			#t.start()
			KyssModule.modules[module].setup()
			
		self.call_ShowMainPage(self.current_page)
		self.log("Start Call Event Loop...")
		while True:
			name, callback, args, new_thread = self.threadQueue.get()
			if name == "STOP":
				break
			if new_thread:
				self.log("Star new thread {} {}...".format(name, callback))
				t = threading.Thread(name=name, target=callback, args=args)
				self.modules_threads[name] = t
				t.start()
			else:
				self.log("Call {} {}...".format(name, callback))
				KyssModule.modules[name].call(callback, args)

	
	
	def onClosing(self):
		self.setConfig("app", "size", "{}x{}".format(KyssModule.main_window.width, KyssModule.main_window.height))
		self.setConfig("app", "lang", KyssModule.lang)
		self.saveConfig()
	
	def onClosed(self):
		self.stopModules()
		self.log("Bye...")
		self.messagesQueue.put(("stop", "", "", "", ""))
		self.exit()

	def start(self, params):
		min_width, min_height = params["webview_geometry"].split("x")
		size = self.getConfig("app", "size")
		if size:
			width, height = size.split("x")
		else:
			width, height = min_width, min_height
			
		width = round(float(width))
		height = round(float(height))
		min_width = round(float(min_width))
		min_height = round(float(min_height))

		os.environ["QTWEBENGINE_DISABLE_SANDBOX"] = "1"
		
		self.log("Create WebView...")
		try:
			window = webview.create_window(params["name"], icon="data"+os.sep+"icon", js_api=self, width=width, height=height, min_size=(min_width, min_height))
		except:
			window = webview.create_window(params["name"], js_api=self, width=width, height=height, min_size=(min_width, min_height))

		self.window = window
		self.initWebview(webview, self.window)
		self.addWindow(window)
		self.loadModules()

		window.events.loaded += self.onLoaded
		window.events.closed += self.onClosed
		window.events.closing += self.onClosing
		
		if platform.system() == "Windows":
			webview.start(lambda: self.call_Refresh(width, height), http_server=True, gui=params["windows_gui"])
		elif platform.system() == "Linux":
			webview.start(self.call_Refresh, http_server=True, gui=params["linux_gui"], debug=True)
		else:
			webview.start(self.call_Refresh, http_server=True)
		self.onClosed()
