########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2021 Nuno Gonçalves (Ulukyn).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import abc
import json
import venv
import shutil
import urllib3
import certifi
import zipfile
import platform
import subprocess
import configparser
from io import BytesIO

from kyss.utils import KyssUtils, abstractmethod
k = KyssUtils("KyssCli")
config = configparser.ConfigParser()

http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())

def main():
	kyss_dir = os.path.dirname(__file__)

	if not k.isfile("setup.cfg"):
		default_config = configparser.ConfigParser()
		if os.path.isfile(kyss_dir+"/dev/default_setup.cfg"):
			default_config.read(kyss_dir+"/dev/default_setup.cfg")
			with open("setup.cfg", "w") as configfile:
				default_config.write(configfile)

	config.read("setup.cfg")

	options = [cliRun(), cliInstall(), cliModules(), cliEnv(), cliBuild(), cliExit()]

	print(k.bg("green")+k.color("black")+"\n")
	print(k.style("bold")+" "*10+"🔱 Welcome to Kyss 🔱")
	print(k.style("normal"))

	args = sys.argv

	while True:
		if len(args) > 1:
			correct_arg = False
			for i, option in enumerate(options):
				if args[1] == option.getName()[0] or args[1] == option.getName()[1]:
					correct_arg = True
					if not option.getOptions():
						if len(args) > 3:
							option.execute(args[2], args[3:])
						elif len(args) > 2:
							option.execute(args[2])
						else:
							option.execute()
						args = [args[0]]
						break
					elif len(args) > 2:
						option.execute(args[2], args[3:])
						if args[2] == "q":
							args = [args[0]]
						else:
							args = [args[0], args[1]]
						break
					else:
						k.pe("yellow", "   kyss "+option.getName()[1])
						printOptions(option.getOptions())
						arg = getInput("Select option: ")
						args.append(arg)

			if not correct_arg:
				k.perror("Wrong option!")
				args = [args[0]]
		else:
			actions_options = []
			for i, option_name in enumerate(options):
				actions_options.append(option_name.getName())
			printOptions(actions_options)
			arg = getInput("Select option: ")
			args.append(arg)

def printOptions(options):
	print(k.bg("yellow")+"\n"+k.style("normal"))
	for i in range(len(options)):
		option_name = options[i]
		print(k.color("white", "bright")+option_name[0]+") "+option_name[1], end="")
		print(k.color("white", "bright")+k.style("normal")+" "*(12-len(option_name[0]+option_name[1]))+" "+option_name[2], end="")
		if i < len(options)-1:
			print()
	print(k.bg("yellow")+"\n"+k.style("normal"))

def getInput(text, color="normal"):
	k.pe(color, text)
	with k.term.cbreak():
		key = k.term.inkey()
		k.p(color, key)
		return key

def getPython():
	if k.isdir("env"):
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		return context.env_exe
	else:
		return sys.executable

########################################################################
class cliAction(object):

	def __init__(self, config=None):
		self.config = config

	@abstractmethod
	def getName(self):
		return ""

	@abstractmethod
	def execute(self, arg="", args=[]):
		pass

	def getOptions(self):
		return []


########################################################################
class cliRun(cliAction):

	def getName(self):
		return ("r", "run", "Manage start of Kyss App")

	def createStarterScript(self):
		appname = config["metadata"]["name"]
		with open(appname+".py", "w") as f:
			f.write("""
import os
import sys
import urllib3
import certifi
import platform
import subprocess
import configparser

http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())

local_config = configparser.ConfigParser()
local_config.read("kyss.cfg")
remote_config = configparser.ConfigParser()

url = local_config["metadata"]["setup"]
try:
	response = http.request("GET", url, preload_content=False)
except:
	print("Error downloading {}".format(url))
	remote_config.read("kyss.cfg")
else:
	if not response.status == 200:
		print("Error Downloading {}".format(url))
		remote_config.read("kyss.cfg")
	else:
		data = response.read().decode()
		response.release_conn()
		remote_config.read_string(data)
remote_version = float(remote_config["metadata"]["version"])

try:
	local_config.read("kyss.cfg")
	local_version = float(local_config["metadata"]["version"])
except:
	local_version = 0

print("Kyss version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version:
	with open("kyss.cfg", "w") as f:
		f.write(data)
		kyss_config = remote_config
	if platform.system() == "Darwin":
		subprocess.run(["curl", local_config["metadata"]["download"], "--output", "kyss.zip"])
		subprocess.run(["rm", "-rf", "kyss-main"])
		subprocess.run(["unzip", "kyss.zip"])
		os.chdir("kyss-main")
		os.remove("setup.cfg")
		subprocess.run([sys.executable, "setup.py", "install", "--force"])
		os.chdir("..")
		subprocess.run(["rm", "-rf", "kyss-main"])
	else:
		subprocess.run([sys.executable, "-m", "pip", "uninstall", "-y", "kyss"])
		subprocess.run([sys.executable, "-m", "pip", "install", "--upgrade", local_config["metadata"]["download"]])
else:
	kyss_config = local_config


local_config = configparser.ConfigParser()
local_config.read("setup.cfg")
remote_config = configparser.ConfigParser()

url = local_config["metadata"]["setup"]
try:
	response = http.request("GET", url, preload_content=False)
except:
	print("Error downloading A {}".format(url))
	remote_config.read("setup.cfg")
else:
	if not response.status == 200:
		print("Error Downloading {}".format(url))
		remote_config.read("setup.cfg")
	else:
		data = response.read().decode()
		response.release_conn()
		remote_config.read_string(data)

remote_version = float(remote_config["metadata"]["version"])

try:
	local_config.read("setup.cfg")
	local_version = float(local_config["metadata"]["version"])
except:
	local_version = 0

app_name = local_config["metadata"]["name"]
app_name = app_name[0].upper()+app_name[1:]
print(app_name, "version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version:
	with open("setup.cfg", "w") as f:
		f.write(data)
	config = remote_config
else:
	config = local_config

os.chdir("app")
sys.path.append(os.getcwd())

from kyss import kyss
kysspy = kyss.Kyss(config, kyss_config)
kysspy.start()
""")

	def execute(self, arg="", args=[]):
		script = config["metadata"]["name"]+".py"
		if not k.isfile(script):
			self.createStarterScript()
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		cmd = [context.env_exe, script]+sys.argv[2:]
		k.pexecute(cmd)
		sys.exit(0)

########################################################################
class cliInstall(cliAction):

	def getName(self):
		return ("i", "install", "Install the Kyss App")

	def execute(self, arg="", args=[]):
		pass

########################################################################
class cliEnv(cliAction):

	def getName(self):
		if not k.isdir("env"):
			return ("e", "env", "Create the Python Virtual Env")
		return ("e", "env", "Manage python virtual env")

	def getOptions(self):
		if not k.isdir("env"):
			return []
		return [
			("r", "remove", "Remove the Python Virtual Env"),
			("l", "list", "List packages"),
			("i", "install", "Install a  package"),
			("k", "kyss", "Install kyss in editable mode"),
			("p", "pywebview", "Install pywebview in editable mode"),
			("d", "dev", "Install a package in editable mode"),
			("s", "shell", "Run interactive shell"),
			("q", "quit", "Quit")]

	def execute(self, arg="", args=[]):
		if not k.isdir("env"):
			self.create()
			return

		if arg == "r" or arg == "remove":
			self.create()
		elif arg == "l" or arg == "list":
			self.getList()
		elif arg == "i" or arg == "install":
			self.install(args)
		elif arg == "k" or arg == "kyss":
			self.kyss()
		elif arg == "p" or arg == "pywebview":
			self.pywebview()
		elif arg == "d" or arg == "dev":
			self.dev()
		elif arg == "s" or arg == "shell":
			self.shell()
		elif arg == "q" or arg == "quit":
			return
		else:
			k.perror("Unknow option: {}".format(arg))
			sys.exit(1)

	def getList(self, arg="", args=[]):
		cmd = [getPython(), "-m", "pip", "list"]
		k.pexecute(cmd)
		input("Please press enter to back to menu...")

	def kyss(self, arg="", args=[]):
		if k.isdir("../kyss"):
			kyss_path = "../kyss"
		else:
			kyss_path = input("Please enter path to your Kyss clone:")
		current_path = os.getcwd()
		python_exe = os.getcwd()+"/"+getPython()
		os.chdir(kyss_path)
		cmd = [python_exe, "-m", "pip", "install",  "--no-warn-script-location", "--editable", "."]
		k.pexecute(cmd)
		os.chdir(current_path)

	def create(self, arg="", args=[]):
		if k.isdir("env"):
			k.pexec("Removing Python Virtual Env")
			shutil.rmtree("env")
			k.pdone()
			return
		k.pexec("Creating Python Virtual Env")
		venv.main(("env", ))
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		cmd = [context.bin_path+"/pip", "install", "-U", "pip"]
		k.execute(cmd)

		k.pdone()
		k.pexec("Upgrading Pip")
		cmd = [context.env_exe, "-m", "pip", "install", "--upgrade", "pip"]
		k.execute(cmd)
		k.pdone()
		k.pexec("Installing Requirements, Please wait")
		with open("requirements.txt", "w") as f:
			f.write(config["options"]["install_requires"])
		cmd = [context.env_exe, "-m", "pip", "install", "-v", "-r", "requirements.txt"]
		k.execute(cmd)
		k.pdone()
		k.pexec("Installing Dist Requirements, Please wait")
		with open("requirements.txt", "w") as f:
			f.write(config["options.extras_require"]["DIST"])
		cmd = [context.env_exe, "-m", "pip", "install", "-v", "-r", "requirements.txt"]
		k.execute(cmd)
		k.pdone()
		os.unlink("requirements.txt")

	def shell(self, arg="", args=[]):
		cmd = ["xterm", "-hold", "-e", "bash -i <<< 'source env/bin/activate; exec </dev/tty'"]
		k.pexecute(cmd)


########################################################################
class cliModules(cliAction):

	def getName(self):
		return ("m", "modules", "Show Modules options")

	def execute(self, arg="", args=[]):
		if arg == "l" or arg == "list":
			self.getList()
		elif arg == "a" or arg == "add":
			self.add()
		elif arg == "i" or arg == "install":
			self.install(args)
		elif arg == "q" or arg == "quit":
			sys.exit(0)
		else:
			k.perror("Unknow option: {}".format(arg))
			sys.exit(1)

	def getOptions(self):
		options = [("l", "list", "List modules"),
			("a", "add", "Add a new module to Kyss App"),
			("i", "install", "Install a module")]

		have_git = k.isdir(".git")
		if have_git:
			options.append(("d", "dev", "Install a module in dev mode"))

		options += [("u", "update", "Update a module or all using *"),
			("r", "remove", "Remove a module in app.ini"),
			("q", "quit", "Quit")]
		return options

	def getList(self):
		pass

	def getPipList(self):
		python = getPython()
		cmd = [getPython(), "-m", "pip", "list", "--format=json"]
		(stdout, stderr) = k.execute(cmd)
		packages = json.loads(stdout)
		final = {}
		for package in packages:
			final[package["name"]] = package["version"]
		return final

	def add(self):
		name = input("Please enter the name of module:")
		url = input("Please enter the url of module.ini file:")
		config = configparser.ConfigParser()
		config.read("app.ini")
		config["MODULES"][name] = url
		with open("app.ini", "w") as configfile:
			config.write(configfile)
		gitClone(name, url)

	def clone(self, name, url):
		k.pexec("Cloning module {}".format(name))
		cmd = ["git", "clone", url.split("/-/")[0], k.path("modules/"+name)]
		k.execute(cmd)

	def dev(self, args):
		self.install(args, True)

	def install(self, args, dev=False):
		installed_packages = self.getPipList()
		if not args:
			k.ptitle("Installation of module")
			have_appini = k.isfile("app.ini")
			if have_appini:
				k.pdone("app.ini found, installation with names available")
			print()
			print("Please enter one or more modules separated by spaces:")
			if have_appini:
				k.pinfo("   using name if declared in app.ini (separated by space)")
			k.pe("yellow", "or")
			k.pinfo(" using url to the module.ini file of a kyss module")
			k.pe("yellow", "or")
			k.pinfo(" using location to a kyss module folder with module.ini")
		else:
			for arg in args:
				if arg[0:4] == "http":
					config = configparser.ConfigParser()
					module_ini = k.download(arg)
					if module_ini:
						try:
							config.read_string(module_ini.decode())
							version = config["module"]["version"]
							name = config["module"]["name"]
							package = config["module"]["download"]
						except Exception as e:
							k.perror("Invalid module.ini : {}".format(e))
							continue

						k.pcheck("Checking installation of {}".format(k.color("info")+name+k.color("check")))

						if not name in installed_packages or installed_packages[name] != version:
							if installed_packages[name] != version:
								k.pwarning("Package version missmatch : {} vs {}".format(installed_packages[name], version))
							else:
								k.pwarning("Package not installed")
							self.installPackageWithUrl(package, dev)
						else:
							k.pdone("Package installed and up to date")


	def installPackageWithUrl(self, url, dev=False):
		k.pexec("Installing module {}".format(url))
		if dev:
			cmd = [k.path(getPython()), "-m", "pip", "-e", "install", url, "--no-warn-script-location"]
		else:
			cmd = [k.path(getPython()), "-m", "pip", "install", url, "--no-warn-script-location"]
		k.execute(cmd)

	def installPackageInFolder(self, url, dev=False):
		k.pexec("Installing module {}".format(name))
		cwd = os.getcwd()
		os.chdir(k.path("modules/"+name))
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		cmd = [context.bin_path+"/pip", "install", ".", "--no-warn-script-location"]
		k.execute(cmd)
		os.chdir(cwd)


class cliBuild(cliAction):

	def getName(self):
		return ("b", "build", "Show Build Packages options")

	def execute(self, arg="", args=[]):
		if not k.isdir("env"):
			k.perror("Please create env before..")
			sys.exit(1)

		if not k.isdir("kyss"):
			dist = k.download("https://gitlab.com/ulukyn/kyss-dev/-/archive/main/kyss-dev-main.zip")
			filebytes = BytesIO(dist)
			myzipfile = zipfile.ZipFile(filebytes)
			myzipfile.extractall(".")
			os.rename("kyss-dev-main", "kyss")

		if arg == "w" or arg == "windows":
			cmd = ["xterm", "-hold", "-e", "bash "+os.getcwd()+"/kyss/windows/build.sh 64"]
		elif arg == "i" or arg == "windows32":
			cmd = ["xterm", "-hold", "-e", "bash "+os.getcwd()+"/kyss/windows/build.sh 32"]
		elif arg == "l" or arg == "linux":
			cmd = ["xterm", "-hold", "-e", "bash "+os.getcwd()+"/kyss/linux/build.sh"]
		elif arg == "q" or arg == "quit":
			return
		else:
			k.perror("Unknow option: {}".format(arg))
			sys.exit(1)
		k.execute(cmd)

	def getOptions(self):
		if not k.isdir("env"):
			return []
		return [
			("i", "windows32", "Build Windows 32 package"),
			("w", "windows", "Build Windows 64 package"),
			("l", "linux", "Build Linux package"),
			("q", "quit", "Quit")]


class cliExit(cliAction):

	def getName(self):
		return ("q", "quit", "Quit kyss")

	def execute(self, arg="", args=[]):
		k.p("yellow", "Goodbye!")
		sys.exit(0)

