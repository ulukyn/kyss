########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import configparser

from kyss.utils import KyssUtils
ku = KyssUtils("KyssConfig")

class KyssConfig:
	
	def __init__(self, filename):
		self.file = filename
		self.config = configparser.ConfigParser()
		self.refresh()
		self.fileExists = False
	
	def refresh(self):
		if os.path.isfile(self.file):
			self.fileExists = True
			try:
				self.config.read(self.file)
			except:
				try:
					self.config.read(self.file, encoding="utf-8-sig")
				except Exception as e:
					ku.error("ini file {} contains errors:".format(self.file))

	def hasSection(self, section):
		return self.config.has_section(section)

	def getSections(self):
		return self.config.sections()
	
	def reset(self):
		self.config = configparser.ConfigParser()

	def get(self, section, name="", default=None):
		if section in self.config.sections():
			if not name:
				return self.config[section]
			return self.config.get(section, name, fallback=default)
		return default
	
	def set(self, section, name, value):
		if not section in self.config:
			self.config[section] = {}
		self.config[section][name] = str(value)
		return self
	
	def remove(self, section, name=None):
		if name == None:
			self.config.remove_section(section)
		else:
			self.config.remove_option(section, name)

	def save(self):
		path = os.path.dirname(self.file)
		if not os.path.isdir(path):
			os.makedirs(path)
		with open(self.file, "w") as configfile:
			self.config.write(configfile)
