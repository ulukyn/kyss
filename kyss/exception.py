########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import sys
import datetime
import traceback
import configparser
from urllib import parse, request

class KyssExceptionHandler:
	
	def run(self, exc_type, exc_value, exc_traceback):
		if issubclass(exc_type, KeyboardInterrupt):
			sys.__excepthook__(exc_type, exc_value, exc_traceback)
			return
		
		infos = traceback.format_exception(exc_type, exc_value, exc_traceback)
		message = "\033[31m=== ERROR: Kyss exception log ===\n\033[34m{}\n\033[36m{}\033[32m".format(datetime.datetime.now(), infos[-1])
		print(message, end="")
		sys.stderr.write(message+"\n")
		for info in infos[1:-1]:
			print("\033[33m"+info, end="")
			sys.stderr.write("\033[33m"+info+"\n")
		print("\033[31m=================================\033[39m")
		sys.stderr.write("\033[31m=================================\033[39m\n")
		sys.stderr.flush()

