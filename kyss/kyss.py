########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import json
import time
import glob
import shutil
import certifi
import urllib3
import platform
import argparse
import importlib
import subprocess
import configparser

from subprocess import Popen, PIPE
from contextlib import redirect_stdout

from kyss.utils import KyssUtils
from kyss.module import KyssModule
from kyss.updater import KyssUpdater

import kyss.logger

import kyss.progress.tk as KyssTkProgress
import kyss.progress.echo as KyssEchoProgress

if KyssTkProgress.tkAvailable:
	KyssProgress = KyssTkProgress.KyssTkProgress
else:
	import kyss.progress.zenity as KyssZenityProgress
	if KyssZenityProgress.zenityAvailable:
		KyssProgress = KyssZenityProgress.KyssZenityProgress
	else:
		KyssProgress = KyssEchoProgress

python_version = "{}.{}".format(sys.version_info.major, sys.version_info.minor)

k = KyssUtils()

from kyss.exception import KyssExceptionHandler
sys.excepthook = KyssExceptionHandler().run

class Kyss:
	kyssModule = None
	
	def __init__(self, config, kyss_config):
		self.config = config
		self.kyss_config = kyss_config
		self.argparser = argparse.ArgumentParser()

	def init(self):
		### Build a nice sys.path
		if os.path.isdir("python"):
			# It's an installation. Clean the sys.path. Only childs path or current dir can be used
			sys_path = []
			for path in sys.path:
				if path[:len(os.getcwd())] == os.getcwd():
					sys_path.append(path)
			sys.path = sys_path
		sys.path.append(os.getcwd())
		sys.path.append(os.getcwd()+os.path.sep+"modules")

		self.params = {}
		self.params["name"] = self.config.get("metadata", "name", fallback="MyKyssApp")
		self.params["description"] = self.config.get("metadata", "description", fallback="My Kyss App")
		self.params["version"] = self.config.get("metadata", "version", fallback="1.0")
		self.params["url"] = self.config.get("metadata", "url", fallback="")
		self.params["download"] = self.config.get("metadata", "download", fallback="")

		self.params["author"] = self.config.get("author", "name", fallback="me")
		self.params["author_email"] = self.config.get("author", "email", fallback="me@me.me")
		
		self.params["windows_gui"] = self.config.get("kyss.options", "windows_gui", fallback="cef")
		self.params["linux_gui"] = self.config.get("kyss.options", "linux_gui", fallback="qt")
		self.params["webview_geometry"] = self.config.get("kyss.options", "webview_geometry", fallback="1000x600")
		
		k.log("Init Module with app infos")
		KyssModule.setAppInfos(self, self.params["name"], self.params["author"], self.params["version"])
		KyssModule.langs = {x[0] : x[1] for x in self.config.items("kyss.langs")}

		self.argparser.add_argument("-e", help="path of ryztart executable", default="")
		self.argparser.add_argument("-i", "--install", type=int, choices=[0, 1], help="start installation process if enabled", default=0)
		self.argparser.add_argument("-l", "--lang", choices=["en", "fr", "de", "es", "ru"], help="select lang", default="")
		self.argparser.add_argument("-t", "--updatetrs", type=int, choices=[0, 1], help="update translation files", default=0)
		if platform.system() == "Windows":
			self.argparser.add_argument("-s", "--show", type=int, choices=[0, 1], help="show the console", default=0)
		self.args = self.argparser.parse_args()
		KyssModule.args = self.args
		
		if self.args.lang:
			KyssModule.lang = self.args.lang
		else:
			KyssModule.setAndGetLang()

		if platform.system() == "Windows" and self.args.show == 0:
			import win32gui
			import win32.lib.win32con as win32con
			def callback(hwnd, extra):
				name = win32gui.GetWindowText(hwnd).lower()
				if os.path.basename(os.path.abspath(os.path.dirname(__file__)+"\\..\\..\\..")).lower() in name and "python.exe" in name:
					win32gui.ShowWindow(hwnd , win32con.SW_HIDE)
			win32gui.EnumWindows(callback, None)

	def install(self):
		self.install_dir = os.path.abspath(os.getcwd()+os.sep+"..")
		if self.args.install == 1 and platform.system() == "Linux":
			from kyss.installer_linux import KyssInstallerLinux
			
			kil = KyssInstallerLinux(self.params)
			kil.create()
			print(k.c("cyan"), "-=== START INSTALLATION", k.c("reset"))
			kil.startInstallation(lambda progress, install_dir : kil.install(progress, install_dir))
			os._exit(1)


	def update(self):
		try:
			KyssProgress(lambda progress, params: self.updatePackages(progress))
		except Exception as e:
			k.error("{}".format(e))
			KyssEchoProgress(lambda progress, params: self.updatePackages(progress))
		
		if self.args.updatetrs == 1:
			self.updateI18n()

	def updatePackages(self, progress):
		updater = KyssUpdater()
		k.log("-=== Update packages at {} ===-".format(self.install_dir))
		updater.updatePackages(self.config, self.kyss_config, progress)
		k.log("Installing QT5 xcb required libs")
		# Libs required by QT5
		files = glob.iglob(os.path.join("python", "usr", "lib", "libxcb-*"))
		dst = "python/opt/python"+python_version+"/lib/python/"+python_version+"/site-packages/PyQt5/Qt5/lib"
		for xcblib in files:
			if os.path.isfile(xcblib) and os.path.isdir(dst):
				k.log("Copy "+xcblib+"...")
				shutil.copy2(xcblib, dst)

		if os.path.isdir(".."+os.sep+".git"):
			k.log("DEV MODE : NO SELF PATCH")
		else:
			k.log("-=== Update App at {} ===-".format(self.install_dir))
			updater.updateApp(self.params, progress)
		k.log("Update finished")
		progress.setValue(100, "OK")

	def getTranslations(self, lang, src, default=None):
		if default:
			i18n_sentences = default.copy()
		else:
			i18n_sentences = {}
		if src[:8] == "https://": # downloadable file
			http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED", ca_certs=certifi.where())
			try:
				response = http.request("GET", src, preload_content=False)
			except:
				return
			if response.status == 200:
				mode = "single"
				for line in response.read().decode("utf-8").replace("\r", "").split("\n"):
					if line:
						if mode == "single":
							i18n_fullname, i18n_sentence = line.split("\t", 2)
							if "." in i18n_fullname:
								i18n_module, i18n_name = i18n_fullname.split(".")
							else:
								i18n_module = ""
								i18n_name = i18n_fullname
							if i18n_sentence[-1] == "]":
								if not i18n_module in i18n_sentences:
									i18n_sentences[i18n_module] = {}
								if i18n_sentence != "[]":
									i18n_sentences[i18n_module][i18n_name] = i18n_sentence[1:-1]
							else:
								mode = "multi"
						else:
							if line[-1] == "]":
								mode = "single"
								i18n_sentence += line[1:]
								i18n_sentence = i18n_sentence.replace("\\n", "\n")
								if not i18n_module in i18n_sentences:
									i18n_sentences[i18n_module] = {}
								if i18n_sentence != "[]":
									i18n_sentences[i18n_module][i18n_name] = i18n_sentence[1:-1]
							else:
								i18n_sentence += line[1:]
		return i18n_sentences


	def updateI18n(self):
		if "wk" in KyssModule.langs:
			wk_sentences = self.getTranslations("wk", KyssModule.langs["wk"])
		
		for lang, src in KyssModule.langs.items():
			if lang != "wk" and lang != "default":
				i18n_sentences = self.getTranslations("wk", src, wk_sentences)
				for i18n_module, i18n_lines in i18n_sentences.items():
					i18n_path = os.path.join("data", "i18n", lang)
					if not os.path.isdir(i18n_path):
						os.makedirs(i18n_path)
					if i18n_module:
						with open(i18n_path+os.sep+i18n_module+".data", "w") as f:
							f.write(json.dumps(i18n_lines))

	def start(self):
		self.init()
		self.install()
		self.update()

		from kyss.app import KyssApp
		kapp = KyssApp("app")
		kapp.start(self.params)
