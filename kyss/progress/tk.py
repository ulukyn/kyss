########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
from kyss.utils import KyssUtils

k = KyssUtils("KyssTkProgress")
k.log("Trying Tk...")

try:
	import tkinter as tk
	from PIL import ImageTk, Image
except:
	k.log("Tk not available")
	tkAvailable = False
else:
	k.log("Tk available")
	tkAvailable = True

if tkAvailable:
	class KyssTkProgress(tk.Tk):
		
		def __init__(self, callback, params=""):
			tk.Tk.__init__(self)
			self.title("Kyss Self Updater")
			
			# Image
			img = ImageTk.PhotoImage(Image.open("data"+os.sep+"installer.jpg"))
			label = tk.Label(self, image=img, bd=0)
			label.image = img
			label.pack(side="top")
			
			self.frame = tk.Frame(self, bg="#222")
			self.frame.pack(side="top", fill="both", expand=True)
			progress = KyssTkProgressBar(self, img.width())
			self.after(100, lambda: callback(progress, params))
			self.mainloop()
		
		def finish(self):
			try:
				self.destroy()
				self.finish()
			except:
				pass
	
	class KyssTkProgressBar(tk.Canvas):
		
		def __init__(self, root, width=200, height=80, color="#1E90FF"):
			tk.Canvas.__init__(self, root.frame, bg="#070707", highlightthickness=0)
			self.root = root
			self.pack(side="top")
			self.active = True
			self.lastValue = -1
			self.parent = root.frame
			self.width = width
			self.height = height
			
			self.configure(height=height, width=width)
			fill_out = self.create_rectangle(0, 0, 0, height, fill="#333", outline=color)
			self.coords(fill_out, (15, 15, width-15, 35))
			self.fill_rec = self.create_rectangle(0, 10, 0, 10+height, fill=color, outline=color)
			self.coords(self.fill_rec, (15, 15, 0, 35))
			self.info = self.create_text(width/2, 10+height/2, font="Lucida 9", text="Preparing self update...", fill="white")
			self.value = 0

		def setValue(self, value, message=""):
			if not self.active:
				return
			
			if value == self.lastValue:
				return
				
			# Something bad...
			if value > 100:
				return
			
			self.lastValue = value
			self.coords(self.fill_rec, (15, 15, ((self.width-15)*value)/100, 35))
			self.delete(self.info)
			self.info = self.create_text(self.width/2, 10+self.height/2, font="Lucida 9", text=message, fill="white")
			self.parent.update()

			if value == 100:
				self.finish()
		
		def finish(self):
			self.root.finish()

