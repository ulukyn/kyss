########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import sys
import stat
import shutil
import hashlib
import platform
import importlib
import subprocess
import configparser


from distutils.dir_util import copy_tree
from packaging.version import Version

from pathlib import Path
from kyss.module import KyssModule
from kyss.utils import KyssUtils
from .utils import *

k = KyssUtils("KyssInstaller")

class KyssUpdater(KyssModule):

	def getUsedZones(self):
		return {"footer": 1}

	def _pythonPath(self):
		if platform.system() == "Linux":
			return "/python/opt/python"+python_version+"/lib/python"+python_version+"/site-packages"
		elif platform.system() == "Windows":
			return "python\\Lib\\site-packages"

	def _getPipList(self, install_dir):
		call = [sys.executable, "-m", "pip", "list"]
		process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = process.communicate()
		return out.decode("utf-8").replace("\r", "")

	def _installWithPip(self, uninstall_packages, install_packages, url_packages, install_dir):
		if uninstall_packages:
			subprocess.call([sys.executable, "-m", "pip", "uninstall", "-y"]+uninstall_packages)
		if install_packages:
			subprocess.call([sys.executable, "-m", "pip", "install", "--upgrade"]+install_packages)
		if url_packages:
			for url in url_packages:
				if platform.system() == "Darwin":
					filename = os.path.basename(url.replace("https://", ""))
					subprocess.run(["rm", "-rf", "tmp_pkg"])
					os.mkdir("tmp_pkg")
					os.chdir("tmp_pkg")
					with open(filename, "wb") as f:
						f.write(k.download(url))
					subprocess.run(["unzip", filename])
					os.remove(filename)
					os.chdir(os.listdir(".")[0])
					subprocess.run([sys.executable, "setup.py", "install", "--force"])
					os.chdir("../..")
					subprocess.run(["rm", "-rf", "tmp_pkg"])
				else:
					subprocess.call([sys.executable, "-m", "pip", "install", "--upgrade", url])
				
				

	def updatePackages(self, config, kyss_config, progress, install_dir=""):
		if not install_dir:
			install_dir = os.path.abspath(os.getcwd()+os.sep+"..")

		if platform.system() != "Darwin":
			sys.path.append(install_dir+self._pythonPath())

		packages = {}

		for line in kyss_config["options.extras_require"]["DIST"].split("\n"):
			if line and not "platform_system" in line or platform.system() in line:
				sline = re.split("[ ><=]+", line, 3)
				if sline[1] == "@":
					sline[1] = sline[2].split("/")[-1][:-4]+"@"+sline[2]
				packages[sline[0]] = sline[1].replace(";", "")

		for line in config["options.extras_require"]["DIST"].split("\n"):
			if line and not "platform_system" in line or platform.system() in line:
				sline = re.split("[ ><=]+", line, 3)
				if sline[1] == "@":
					sline[1] = sline[2].split("/")[-1][:-4]+"@"+sline[2]
				packages[sline[0]] = sline[1].replace(";", "")

		pos = 98-(2*len(packages.keys()))
		local_packages = {}
		self.log("Local packages:")
		for line in self._getPipList(install_dir).split("\n")[2:]:
			sline = re.split(r"\s+", line)
			if len(sline) == 2:
				local_packages[sline[0].lower()] = sline[1]
				self.log("\t"+sline[0].lower()+" = "+sline[1])
		install_packages = []
		uninstall_packages = []
		url_packages = []
		progress.setValue(pos, "Checking packages...")
		self.log("Wanted packages:")
		
		if platform.system() == "Darwin":
			packages = {"pywebview": packages["pywebview"]}
		
		for package, package_vesion in packages.items():
			self.log("\t"+package+" = "+package_vesion)
			uninstall_before = False
			package_url = ""

			if package_vesion[0] == "!":
				package_vesion = package_vesion[1:]
				uninstall_before = True

			if "@" in package_vesion:
				package_vesion, package_url = package_vesion.split("@")

			if not package in local_packages:
				local_packages[package] = "0"

			pos += 1
			package_v = Version(package_vesion)
			local_v = Version(local_packages[package])
			if package_v > local_v:
				progress.setValue(pos, "Installing/Updating {} into {}. Please wait...".format(package[0].upper()+package[1:], install_dir))

				install_package = package+"=="+package_vesion

				if uninstall_before:
					uninstall_packages.append(package)

				if package_url:
					url_packages.append(package_url)
				else:
					install_packages.append(install_package)
			pos += 1
		if install_packages or url_packages:
			self._installWithPip(uninstall_packages, install_packages, url_packages, install_dir)
			if platform.system() == "Linux":
				os.execvp("../"+config["metadata"]["name"], [config["metadata"]["name"]])


	def updateApp(self, params, progress, install_dir=""):
		app_name = params["name"]
		if not install_dir:
			install_dir = os.path.abspath(os.getcwd()+os.sep+"..")
		files_infos = []
		files_hashes = []
		files_rm = []
		url = params["download"][:35]
		surl = params["download"].split("/")
		branch = surl[7]
		if url[:18] == "https://gitlab.com":
			k.log("Getting files from gitlab {} with branch {}".format(url, branch))
			
			files_infos = self.downloadFile(url+"raw/"+branch+"/files.info")
			if files_infos:
				files_infos = files_infos.decode().split("\n")
			else:
				return
				
			files_hashes = self.downloadFile(url+"raw/"+branch+"/files.sha1")
			
			if files_hashes:
				files_hashes = files_hashes.decode().split("\n")
			else:
				return
			
			files_rm = self.downloadFile(url+"raw/"+branch+"/files.rm")
			if files_rm:
				files_rm = files_rm.decode().split("\n")
			else:
				return

		for filename in files_rm:
			if os.path.isfile(filename):
				progress.setValue(10, "Removing File {}...".format(filename))
				k.log("Removing {}...".format(filename))
				os.remove(filename)
			elif os.path.isdir(filename):
				os.rmdir(filename)

		files_sha1 = {}
		for files_hash in files_hashes:
			sline = files_hash.split(" ")
			if len(sline) == 2:
				files_sha1[sline[1]] = sline[0]

		total = len(files_infos)
		processed = 0
		for file_info in files_infos:
			infos = file_info.split(" ")
			processed += 1

			if len(infos) > 2 and infos[0][-4:] != ".tmp" and not "__pycache__" in infos[0]:
				need_log = False
				update_it = False
				if os.path.isfile(infos[0]):
					stats = os.stat(infos[0])
					if int(infos[1]) != stats.st_size or int(infos[2]) != stats.st_mtime:
						if int(infos[1]) != stats.st_size:
							k.log("{} size diff: {} vs {}...".format(infos[0], stats.st_size, infos[1]))
							update_it = True
				else:
					update_it = True

				if update_it and infos and infos[0] in files_sha1:
					progress.setValue(round((90*processed)/total), "Updating File {}...".format(infos[0]))
					k.log("Updating {}...".format(infos[0]))

					dirname = os.path.dirname(infos[0])
					if dirname and not os.path.isdir(dirname):
						k.log("Creating folder {}".format(dirname))
						os.makedirs(dirname)

					try:
						file_content = self.downloadFile(url+"raw/"+branch+"/app/"+infos[0])
						with open(infos[0]+".tmp","wb") as f:
							f.write(file_content)
					except:
						continue

					with open(infos[0]+".tmp", "rb") as f:
						downloaded_sha1 = hashlib.sha1(f.read()).hexdigest()

					if downloaded_sha1 == files_sha1[infos[0]]:
						k.log("Patched successfuly!")
						shutil.copyfile(infos[0]+".tmp", infos[0])
					else:
						#Todo: manage this error better
						k.error("Patched corrumpted...")

					try:
						os.remove(infos[0]+".tmp")
					except:
						pass
					
					if infos[0] == app_name+".py":
						try:
							shutil.copyfile(app_name+".py", "../"+app_name+".py")
						except:
							pass

				# Update modification time
				stats = os.stat(infos[0])
				os.utime(infos[0], (int(infos[2]), int(infos[2])))
				stats = os.stat(infos[0])
