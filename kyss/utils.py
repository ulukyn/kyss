########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2021 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import urllib
import urllib3
import certifi
import platform
import traceback
import subprocess
from pathlib import Path
from blessed import Terminal
from inspect import getframeinfo, stack

terminal_colors =  Terminal().number_of_colors
if terminal_colors == 0:
	if platform.system() == "Linux":
		os.environ["TERM"] = "linux"

python_version = "{}.{}".format(sys.version_info.major, sys.version_info.minor)

def abstractmethod(method):
	"""
	An @abstractmethod member fn decorator.

	"""
	def default_abstract_method(*args, **kwargs):
		raise NotImplementedError('call to abstract method ' 
								  + repr(method))
	default_abstract_method.__name__ = method.__name__    
	return default_abstract_method

class KyssUtils:
	
	def __init__(self, id="Kyss"):
		self.id = id
		self.colors = {
			"info" : "white",
			"check": "yellow",
			"error": "red",
			"done": "green",
			"exec": "cyan",
			"title": "yellow",
			"warning": "magenta",
			}
		self.term = Terminal(stream=sys.stderr)
		self.http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())

	def isdir(self, *path):
		return Path(os.path.join(*path)).is_dir()
	
	def isfile(self, *path):
		return Path(os.path.join(*path)).is_file()

	def fullpath(self, *path):
		return Path(os.path.join(*path)).resolve()
	
	def path(self, *path):
		return str(Path(os.path.join(*path)))

	def p(self, color, text, end="\n"):
		"""Print colored text
		
		Args:
			color(str): Color name
			text(str): Text to display
			end(str, optional): Printed end char (default is "\n")
		"""
		
		print("{}{}{}".format(self.color(color), text, self.color("normal")), end=end)
		if end != "\n":
			sys.stdout.flush()


	def pe(self, color, text, end=""):
		"""Alias to p() with default end char to ""
		
		Args:
			color(str): Color name
			text(str): Text to display
			end(str, optional): Printed end char (default is "")
		"""
		
		self.p(color, text, end)

	def ptitle(self, text):
		"""Print log like a title
		
		Args:
			text(str): text
		"""
		
		self.p(self.colors["title"], "⚜⚜⚜ {}️ ⚜⚜⚜".format(text))


	def pexec(self, text, end=False):
		"""Print log of a running command
		
		Args:
			text(str): text
			end(str, optional): Use "\n" at end or not (default is False)
		"""
		
		if end:
			self.p(self.colors["exec"], "▶️ "+text+"... ")
		else:
			self.pe(self.colors["exec"], "▶️ "+text+"... ")


	def pcheck(self, text):
		"""Print log of a running check
		
		Args:
			text(str): text
		"""
		
		self.pe(self.colors["check"], "🔦 "+text+"... ")


	def pdone(self, text=""):
		"""Print log of a done task
		
		Args:
			text(str, optional): text (default is False)
		"""
		
		self.p(self.colors["done"], text+" ✅")


	def pinfo(self, text, end=True):
		"""Print log of information
		
		Args:
			text(str): text
			end(str, optional): Use "\n" at end or not (default is False)
		"""

		if end:
			self.p(self.colors["info"], text)
		else:
			self.pe(self.colors["info"], text)

	def pwarning(self, text):
		"""Print log of error
		
		Args:
			text(str): text
		"""

		self.p(self.colors["warning"], text)



	def perror(self, text):
		"""Print log of error
		
		Args:
			text(str): text
		"""

		self.p(self.colors["error"], text)


	def style(self, name):
		"""Set style of text
		
		Args:
			name (str): The name of the style
				Availabe basic styles are :
					bold, reverse, blink, normal
				Available advanced (less commonly supported) styles are :
					dim, underline, no_underline, italic, no_italic, shadow, no_shadow, standout, no_standout, subscript, no_subscript, superscript, no_superscript, flash
		
		Returns:
			str: Formated text
		"""
		
		return ("{t."+name+"}").format(t=self.term)

	def c(self, name, mode="normal"):
		"""Set color of text
		
		Args:
			name (str): The name of the color
				Availabe classic color are : black, red, green, yellow, blue, magenta, cyan, white
				All available colors : https://blessed.readthedocs.io/en/stable/colors.html
			mode (str, optional): Use normal, bright or bold color (default is "normal")
			
		Returns:
			str: Formated text
		"""
		return self.color(name, mode)

	def color(self, name, mode="normal"):
		"""Set color of text
		
		Args:
			name (str): The name of the color
				Availabe classic color are : black, red, green, yellow, blue, magenta, cyan, white
				All available colors : https://blessed.readthedocs.io/en/stable/colors.html
			mode (str, optional): Use normal, bright or bold color (default is "normal")
			
		Returns:
			str: Formated text
		"""
		
		if name in self.colors:
			name = self.colors[name]
		
		if mode == "bright":
			return ("{t.bright_"+name+"}").format(t=self.term)
		if mode == "bold":
			return ("{t.bold_"+name+"}").format(t=self.term)
		return ("{t."+name+"}").format(t=self.term)


	def bg(self, name):
		"""Set background color of text
		
		Args:
			name (str): The name of the background color
				Availabe classic color are : black, red, green, yellow, blue, magenta, cyan, white
				All available colors : https://blessed.readthedocs.io/en/stable/colors.html
		
		Returns:
			str: Formated text
		"""
		return ("{t.on_"+name+"}").format(t=self.term)


	def download(self, url, method="GET", callback=None):
		try:
			response = self.http.request(method, url, preload_content=False)
		except:
			self.perror("Max retry error: {}".format(url))
			return None
		
		if not response.status == 200:
			self.perror("Downloading {}".format(url))
			return None
		if callback:
			final = b""
			while True:
				data = response.read(131072)
				if not data:
					break
				callback(len(data))
				final += data
			response.release_conn()
			return final
		else:
			return response.read()

	def execute(self, command, check=True):
		""" Execute a command using subprocess
		
		Args:
			command(list): the command to execute with args
		
		Returns:
			list: a tuple with stdout and stderr
		"""
		
		try:
			result = subprocess.run(command, capture_output=True, check=check)
		except Exception as e:
			self.perror("\nError in execution of ["+" ".join(command)+"]:\n{}".format(e.stderr))
			return (None, None)
		return (result.stdout, result.stderr)

	def pexecute(self, command, check=True):
		""" Execute a command using subprocess and echo
		
		Args:
			command(list): the command to execute with args

		"""
		
		try:
			subprocess.run(command, universal_newlines=True)
		except Exception as e:
			self.perror("\nError in execution of ["+" ".join(command)+"]:\n{}".format(e.stderr))
			return False
		return True

	def tb(self, log, module=None):
		if not module:
			module = self.id
		print(self.c("yellow"), "[{}]".format(module), self.c("white"), "{}".format(log), self.c("reset"))
		
	def log(self, log, module=None, line="", tb=""):
		if not module:
			module = self.id
		if not line:
			line = getframeinfo(stack()[1][0]).lineno
		print(self.c("yellow"), "[{}:{}]".format(module, line), self.c("green"), "{}".format(log), self.c("reset"))
		sys.stdout.flush()
		
	def warning(self, log, module=None, line="", tb=""):
		if not module:
			module = self.id
		if not line:
			line = getframeinfo(stack()[1][0]).lineno
		print(self.c("magenta"), "[{}:{}]".format(module, line), log, self.c("reset"))
		sys.stdout.flush()

	def error(self, log, module=None, line="", tb=""):
		if not module:
			module = self.id
		if not line:
			line = getframeinfo(stack()[1][0]).lineno
		if tb.strip():
			print(self.c("red"), "="*74, "\n", "[{}:{}]".format(module, line), log, "\n", self.c("white"), tb, self.c("red"), "\n", "="*74, self.c("reset"))
		else:
			print(self.c("red"), "="*74, "\n", "[{}:{}]".format(module, line), log, "\n", "="*74, self.c("reset"))
		sys.stdout.flush()
