import os
import configparser
from setuptools import setup


config = configparser.ConfigParser()
config.read("setup.ini")

with open("requirements.txt") as f:
	required = f.read().splitlines()

if __name__ == "__main__":
	setup(
		name=config["metadata"]["name"],
		description=config["metadata"]["description"],
		url=config["metadata"]["url"],
		download=config["metadata"]["download"],
		version=config["metadata"]["version"],
		author=config["author"]["name"]+" ("+config["author"]["email"]+")",
		long_description="file: README.md",
		keywords="pywebview, build, packaging",
		python_requires=">=3.8",
		package_dir={"kyss": "kyss"},
		packages=["kyss", "kyss.progress"],
		install_requires=required,
	)
